package demo;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class B2cLogin extends TestBase{
	
	WebDriver driver = null;
	String userName= "";
	String passWord="";
	String URL ="";
	
	@Parameters ({"Browser"})
	@BeforeTest
	public void testSetup(String Browser)
	{
		driver = browserSetup(Browser);
		javaLogInit();
	}
	
	@Parameters ({"url", "username", "password"})
	@Test(enabled=true)
	public void Verify_b2c_login_browser(String url, String username, String password)
	{
		try
		{
			URL = url;
			userName = username;
			passWord = password;
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			System.out.println("URL displayed: " + driver.getCurrentUrl());
			
			waitForElement(By.id("aSignIn"));
			driver.findElement(By.id("aSignIn")).click();
			
			System.out.println("Clicked on SIGN IN ---");
			
			//closeFXRatePopup();
			
			 driver.findElement(By.id("UserName")).clear();
			 driver.findElement(By.id("UserName")).sendKeys(username);
			 Thread.sleep(1000L);
			 System.out.println("Entered USERNAME ---" + username);
			 //log4j.log(Priority.INFO, "Username entered: " + stage_username);
			 //Thread.sleep(1000);
				
			 driver.findElement(By.id("Password")).clear();
			 driver.findElement(By.id("Password")).sendKeys(password);
			 System.out.println("Entered PASSWORD ---" + password);
			 //log4j.log(Priority.INFO, "Password entered: " + stage_password);
			 driver.findElement(By.id("InstaValidate")).click();
			 
			 waitForLoaderToDisappear();
			
			if (url.contains("staging"))
			{
				 if (driver.findElement(By.linkText("Make A Payment")).isDisplayed())
					 JLogger.log(Level.INFO, "Stage Login successful: " + username);
				 else
					 JLogger.log(Level.INFO, "Stage Login NOT successful: " + password);
			}
			else
			{
				if (driver.findElement(By.linkText("Make A Payment")).isDisplayed())
					JLogger.log(Level.INFO, "PROD Login successful: " + username);
				else
					JLogger.log(Level.INFO, "PROD Login NOT successful: " + password);
			}

			Thread.sleep(3000L);
			
			logout();
			System.out.println("Clicked LOGOUT ---");
		}
		catch(Exception e)
		{
			String path = takeScreenshot(driver);
			
			if (url.contains("staging"))
				sendMailWithAttachment("Login Exception", "Login Exception for STAGE user: " + userName +"\n\nEXCEPTION: "+ e.getMessage(), path);
			else
				sendMailWithAttachment("Login Exception", "Login Exception for PROD user: " + userName +"\n\nEXCEPTION: "+ e.getMessage(), path);
			
			JLogger.log(Level.SEVERE, "Exception in Verify_b2c_login_browser(): " + e.getMessage());
		}
	}
	
	public void logout()
	 {
		 try
		 {
			 WebElement profileImage =  driver.findElement(By.xpath("//div[@class='welcome-user']/p"));
			 WebElement logoutLink = driver.findElement(By.id("aLogout"));
			 
			 profileImage.click();
			 Thread.sleep(1000L);
			 logoutLink.click();
		 }
		 catch(Exception e)
		 {
			 JLogger.log(Level.SEVERE, "Exception in logout(): " + e.getMessage());
		 }
	 }
	
	@AfterTest
	public void cleanUp()
	{
		driver.close();
		JavaLogFile.flush();
		JavaLogFile.close();
	}

}
