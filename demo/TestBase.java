package demo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {
	
	//Constants declared
	public final static String javaLogFilePath = System.getProperty("user.dir") + "//Reports//";
	public static String imagesPath = System.getProperty("user.dir") + "//Images//";
	
	public static Logger JLogger = null;
	public static FileHandler JavaLogFile=null;
	WebDriver driver = null;
	
	public WebDriver browserSetup(String Browser)
	{
		String driverPath ="";
		
		try
		{
			if (Browser.equalsIgnoreCase("linuxChrome"))
			{
				driverPath = System.getProperty("user.dir") + "//drivers//chromedriver";
				System.setProperty("webdriver.chrome.driver",  driverPath);
			 	ChromeOptions options = new ChromeOptions();
			 	options.setHeadless(true);
			 	options.addArguments("disable-gpu");
			 	options.addArguments("window-size=1200,1100");
			    driver = new ChromeDriver(options);
			}
			else if (Browser.equalsIgnoreCase("windowsChrome"))
			{
				driverPath = System.getProperty("user.dir") + "//drivers//chromedriver1.exe";
				System.setProperty("webdriver.chrome.driver",  driverPath);
				/*ChromeOptions options = new ChromeOptions();
			 	options.setHeadless(true);
			 	options.addArguments("disable-gpu");
			 	options.addArguments("window-size=1200,1100");
				driver = new ChromeDriver(options);*/
				driver = new ChromeDriver();
			}
			else if (Browser.equalsIgnoreCase("linuxFF"))
			{
				driverPath = System.getProperty("user.dir") + "//drivers//geckodriver";
				System.setProperty("webdriver.gecko.driver",  driverPath);
				driver = new FirefoxDriver();
			}
			else if (Browser.equalsIgnoreCase("windowsFF"))
			{
				driverPath = System.getProperty("user.dir") + "//drivers//geckodriver.exe";
				System.setProperty("webdriver.gecko.driver",  driverPath);
				driver = new FirefoxDriver();
			}
			//System.out.println(driverPath);
		}
		catch(Exception e)
		{
			System.out.println("Exception in browserSetup() -- " + e.getMessage());
			e.printStackTrace();
		}
		return driver;
	}

	public void javaLogInit()
	{
		//System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$-7s] %5$s %n");
		JLogger = Logger.getLogger("B2CLogger");

		try
		{
			 String logFileName = javaLogFilePath + "Logger_"+ getDate() +".log";
			 FileHandler JavaLogFile = new FileHandler(logFileName, true);
			 
			 JavaLogFile.setFormatter(new java.util.logging.SimpleFormatter());
				JLogger.setLevel(Level.ALL);
				JLogger.addHandler(JavaLogFile);
				JLogger.setUseParentHandlers(false);
		}
		catch(Exception e)
		{
			System.out.println("Exception in javaLogInit(): " +e.getMessage());
		}	
	}
	
	public void waitForLoaderToDisappear()
	 {
		 try
		 {
			 WebDriverWait wait = new WebDriverWait(driver, 40);
			 wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='overlay']/div[@class='page-loader']")));
			//div[@class='overlay']/div[@class='page-loader']
		 }
		 catch (Exception e)
		 {
			 JLogger.log(Level.SEVERE, "Exception in waitForLoaderToDisappear() : " + e.getMessage()); 
		 }
	 }
	
	protected void waitForElement(By locator)
	{
		  WebDriverWait wait = new WebDriverWait(driver, 40);
		  try
		  {
			  wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			  //log4j.info("Element is visible " + locator);
		  }
		  catch(Exception e)
		  {
			  JLogger.log(Level.SEVERE, "Exception in waitForElement() : " + locator.toString() + e.getMessage());
		  }
	}
	
	public String takeScreenshot(WebDriver driver)
	{
		File dest=null;
		try
		{
			File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			//System.out.println(src.getAbsoluteFile());
			dest = new File (imagesPath + getImageName());
			//System.out.println(dest.getAbsolutePath());
			FileUtils.copyFile(src, dest);
		}
		catch(Exception e)
		{
			//JLogger.log(Priority.ERROR, );
			JLogger.log(Level.SEVERE, "Exception in takeScreenshot(): " + e.getMessage());
			//e.printStackTrace();
		}
		return dest.getAbsolutePath();
	}
	
	public String getImageName()
	{
		String imageName = "Image";
		return (imageName+ getDateTime() + ".png");
	}
	
	public String getDateTime()
	{
		Date now = new Date();
        //String datetimeStr = now.toString();
        SimpleDateFormat format = new SimpleDateFormat("MMM_dd_yyyy_HH_mm_ss zzz");
        //System.out.println(format.format(now));
        return (format.format(now));
	}
	
	public String getDate()
	{
		Date now = new Date();
        //String datetimeStr = now.toString();
        SimpleDateFormat format = new SimpleDateFormat("MMM_dd_yyyy");
        //System.out.println(format.format(now));
        return (format.format(now));
	}
	
	public void sendMailWithAttachment(String subject, String msgBody, String imagePath)
	{
		
		Properties props = new Properties();
		//props.put("mail.smtp.host", "smtp.gmail.com"); //smtp.mailgun.org
		props.put("mail.smtp.host", "smtp.mailgun.org"); //
		//props.put("mail.smtp.starttls.enable", "true");
		//props.put("mail.smtp.user", "insta7135@instarem.com"); //postmaster@alts.instarem.com
		props.put("mail.smtp.user", "postmaster@alts.instarem.com"); //
		//props.put("mail.smtp.password", "Smart100"); //85e2b4e76008dae23dfed0c01ba87eae
		props.put("mail.smtp.password", "85e2b4e76008dae23dfed0c01ba87eae"); 
		//props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");
		
		//System.out.println("Inside send email");
		
		Session session = Session.getDefaultInstance(props,
			    new Authenticator() {
			        protected PasswordAuthentication  getPasswordAuthentication() {
			        return new PasswordAuthentication(
			                    "postmaster@alts.instarem.com", "85e2b4e76008dae23dfed0c01ba87eae");
			                }
			    });
		
		Message message = new MimeMessage(session);
		
		try
		{
			message.setFrom(new InternetAddress("postmaster@alts.instarem.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress("pthakur@instarem.com"));
			message.setSubject(subject);
			BodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText(msgBody);
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			DataSource source = new FileDataSource(imagePath);
			messageBodyPart2.setDataHandler(new DataHandler(source));
			messageBodyPart2.setFileName("Image.png");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart2);
			multipart.addBodyPart(messageBodyPart1);
			message.setContent(multipart);
			Transport transport = session.getTransport("smtp");
			//transport.connect("smtp.gmail.com", "insta7135@gmail.com", "Smart100");
			transport.connect("smtp.mailgun.org", "postmaster@alts.instarem.com", "85e2b4e76008dae23dfed0c01ba87eae");
			Transport.send(message, message.getAllRecipients());
			transport.close();
		}
		catch (Exception e)
		{
			JLogger.log(Level.SEVERE, "Exception in sendMailWithAttachment(): " +e.getMessage());
		}
		
	}
}
