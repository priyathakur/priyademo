package demo;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class New extends TestBase {
	
	WebDriver webDriver=null;

	@Parameters ({"Browser"})
	@Test
	public void TEST1(String Browser) throws Exception
	{
		try
		{
			webDriver = browserSetup(Browser);
			webDriver.get("http://www.google.com");
			
			Thread.sleep(5000L);
			
			System.out.println("Opened the google...");
			System.out.println("Page Title --> " + webDriver.getTitle());
			
			webDriver.close();
		}
		catch(Exception e)
		{
			System.out.println("Exception in TEST1() -- ");
			e.printStackTrace();
			System.out.println("");
		}
	}

}
